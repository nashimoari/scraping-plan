# Мысли по организации скрапинга данных

[[_TOC_]]

## Глоссарий
+ API (Application Programming Interface) - Интерфейс через который осуществляется взаимодействие с 

+ Azure (Microsoft Azure) – облачная платформа компании Microsoft. Предоставляет возможность разработки, выполнения приложений и хранения данных на серверах, расположенных в распределённых дата-центрах.

+ Digital Ocean - Компания предоставляющая дисковые , вычислительные, сетевые ресурсы (в частности VDS)

+ Headless браузер - браузер без GUI. Работает в фоновом режиме, и управляется программно через API. 

+ HTML (от англ. HyperText Markup Language — «язык гипертекстовой разметки») - стандартизированный язык разметки документов в www. Большинство веб-страниц содержат описание разметки на языке HTML (или XHTML). Язык HTML интерпретируется браузерами.

+ HTTP - незащищенный протокол передачи данных

+ HTTPS - Защищенный протокол передачи данных. Использует SSL или TLS для шифрования поверх HTTP.

+ IP - IP адрес

+ JS - Язык программирования. Обычно используется как встраиваемый язык для программного доступа к объектам приложений. Наиболее широкое применение находит в браузерах как язык сценариев для придания интерактивности веб-страницам.

+ Kafka - распределённый программный брокер сообщений.

+ MongoDB - NoSQL база данных используемая для хранения неструктурированных данных в формате json

+ Parsing - Процесс извлечения необходимых данных из сырых данных (например html страницы)

+ Proxy server(Прокси сервер) - Сервис посредник на который отправляются запросы предназначенные для другого хоста. Proxy server осуществляет отправку запроса на хост для которого этот запрос предназначался и отправляет ответ хосту который отправил запрос на proxy server. Таким образом осуществляется сокрытие IP адреса реального пользователя который хочет осуществить запрос к какому-либо хосту.

+ Scraping node - Сервис осуществляющий сбор сырых данных и запись их в хранилище

+ Selenium - Инструмент для автоматизации действий веб-браузера. В большинстве случаев используется для тестирования Web-приложений, но этим не ограничивается. В частности, он может быть использован для решения рутинных задач администрирования сайта или регулярного получения данных из различных источников (сайтов).

+ Scrolling - прокрутка экрана. Некоторые сайты реализую бесконечные страницы с подгрузкой контента при пролистывании страницы до конца.

+ SLA (Service Layer Agreement) - Соглашение об уровне сервиса.

+ SSL - Протокол защиты транспортного уровня. Должен быть заменен на TLS в связи с выявленной уязвимостью CVE-2014-3566.

+ Tor(Тор) - Это система прокси-серверов в которой заранее неизвестно через какой именно прокси сервер пойдет запрос.

+ TLS - Протокол защиты транспортного уровня. Усовершенствованный (по сравнению с SSL) криптографическиий протокол

+ Transform node - Сервис осуществляющий обработку сырых данных из хранилища. Парсит, валидирует и сохраняет подготовленные данных в хранилище

+ VPN - (англ. Virtual Private Network «виртуальная частная сеть») — обобщённое название технологий, позволяющих обеспечить одно или несколько сетевых соединений (логическую сеть) поверх другой сети (например Интернет). 

+ VDS (virtual dedicated server) - Виртуальный выделенный сервер

+ Веб-скрапинг - Технология получения веб-данных путем извлечения их со страниц веб-ресурсов. Веб-скрапинг может быть сделан вручную пользователем компьютера, однако термин обычно относится к автоматизированным процессам, реализованным с помощью кода, который выполняет запросы на целевой сайт.

+ капча - механизм защиты данных от скрапинга путем отображения формы на которой пользователю предлагается решить различны вид задач (переписать цифры/буквы с картинки, решить простой пример изображенный на картинке, выбрать правильный вариант изображенный на картинке) и только после решения задачи предоставляется доступ к данным.

## Схема взаимодействия

![alt text](https://mermaid.ink/svg/eyJjb2RlIjoiZ3JhcGggTFJcbnN1YmdyYXBoIHBpW1B1YmxpYyBJbnRlcm5ldF1cbiAgICBIb3N0QVBJW0hvc3QgOkFQSV1cbiAgICBIb3N0SFRNTFtIb3N0IDpzdGF0aWMgaHRtbF1cbiAgICBIb3N0REhUTUxbSG9zdCA6ZHluYW1pYyBodG1sIHBhZ2VdXG5cbiAgICBzdWJncmFwaCBWRFMxW1ZEUzogRGlnaXRhbE9jZWFuXVxuICAgICAgICBEU19Ib3N0UHJveHlbVkRTICNOIDogUHJveHldXG4gICAgZW5kXG5cbiAgICBzdWJncmFwaCBWRFMyW1ZEUzogQXp1cmVdXG4gICAgICAgIEFfSG9zdFByb3h5W1ZEUyAjTiA6IFByb3h5XVxuICAgIGVuZFxuZW5kXG5cbnN1YmdyYXBoIHZwY1tMb2NhbCBuZXR3b3JrXVxuICBNb25nb0RCXG5cbiAgc3ViZ3JhcGggXCJUcmFuc2Zvcm0gbm9kZXMgc3RhY2tcIlxuICAgIFROW1RyYW5zZm9ybSBub2RlICNOXVxuICBlbmRcblxuICBUTi0tPk1vbmdvREJcblxuICBLYWZrYTo6OmthZmthXG5cbiAgc3ViZ3JhcGggU05fU3RhY2tbXCJTY3JhcCBub2RlcyBzdGFja1wiXVxuICAgIFNOW1NjcmFwIG5vZGUgI05dXG4gIGVuZFxuXG4gIFROLS0-S2Fma2FcblxuICBTTiAtLT4gS2Fma2FcbiAgU04gLS0-IE1vbmdvREJcblxuZW5kXG5cblxuRFNfSG9zdFByb3h5IC0uLT4gSG9zdEFQSVxuRFNfSG9zdFByb3h5IC0uLT4gSG9zdEhUTUxcbkRTX0hvc3RQcm94eSAtLi0-IEhvc3RESFRNTFxuXG5cbkFfSG9zdFByb3h5IC0uLT4gSG9zdEFQSVxuQV9Ib3N0UHJveHkgLS4tPiBIb3N0SFRNTFxuQV9Ib3N0UHJveHkgLS4tPiBIb3N0REhUTUxcblxuU04gLS4tPiBEU19Ib3N0UHJveHlcblNOIC0uLT4gQV9Ib3N0UHJveHlcbiIsIm1lcm1haWQiOnsidGhlbWUiOiJuZXV0cmFsIiwiZmxvd2NoYXJ0Ijp7InJhbmtTcGFjaW5nIjo2MCwibm9kZVNwYWNpbmciOjUwLCJjdXJ2ZSI6ImJhc2lzIn0sInRoZW1lVmFyaWFibGVzIjp7InByaW1hcnlDb2xvciI6IiNlZWUiLCJjb250cmFzdCI6IiMyNmEiLCJzZWNvbmRhcnlDb2xvciI6ImhzbCgyMTAsIDY2LjY2NjY2NjY2NjclLCA5NSUpIiwiYmFja2dyb3VuZCI6IiNmZmZmZmYiLCJ0ZXJ0aWFyeUNvbG9yIjoiaHNsKC0xNjAsIDAlLCA5My4zMzMzMzMzMzMzJSkiLCJwcmltYXJ5Qm9yZGVyQ29sb3IiOiJoc2woMCwgMCUsIDgzLjMzMzMzMzMzMzMlKSIsInNlY29uZGFyeUJvcmRlckNvbG9yIjoiaHNsKDIxMCwgMjYuNjY2NjY2NjY2NyUsIDg1JSkiLCJ0ZXJ0aWFyeUJvcmRlckNvbG9yIjoiaHNsKC0xNjAsIDAlLCA4My4zMzMzMzMzMzMzJSkiLCJwcmltYXJ5VGV4dENvbG9yIjoiIzExMTExMSIsInNlY29uZGFyeVRleHRDb2xvciI6InJnYigyMS4yNSwgMTIuNzUsIDQuMjUpIiwidGVydGlhcnlUZXh0Q29sb3IiOiJyZ2IoMTcuMDAwMDAwMDAwMSwgMTcuMDAwMDAwMDAwMSwgMTcuMDAwMDAwMDAwMSkiLCJsaW5lQ29sb3IiOiIjNjY2IiwidGV4dENvbG9yIjoiIzAwMDAwMCIsImFsdEJhY2tncm91bmQiOiJoc2woMjEwLCA2Ni42NjY2NjY2NjY3JSwgOTUlKSIsIm1haW5Ca2ciOiIjZWVlIiwic2Vjb25kQmtnIjoiaHNsKDIxMCwgNjYuNjY2NjY2NjY2NyUsIDk1JSkiLCJib3JkZXIxIjoiIzk5OSIsImJvcmRlcjIiOiIjMjZhIiwibm90ZSI6IiNmZmEiLCJ0ZXh0IjoiIzMzMyIsImNyaXRpY2FsIjoiI2Q0MiIsImRvbmUiOiIjYmJiIiwiYXJyb3doZWFkQ29sb3IiOiIjMzMzMzMzIiwiZm9udEZhbWlseSI6IlwidHJlYnVjaGV0IG1zXCIsIHZlcmRhbmEsIGFyaWFsIiwiZm9udFNpemUiOiIxNnB4Iiwibm9kZUJrZyI6IiNlZWUiLCJub2RlQm9yZGVyIjoiIzk5OSIsImNsdXN0ZXJCa2ciOiJoc2woMjEwLCA2Ni42NjY2NjY2NjY3JSwgOTUlKSIsImNsdXN0ZXJCb3JkZXIiOiIjMjZhIiwiZGVmYXVsdExpbmtDb2xvciI6IiM2NjYiLCJ0aXRsZUNvbG9yIjoiIzMzMyIsImVkZ2VMYWJlbEJhY2tncm91bmQiOiJ3aGl0ZSIsImFjdG9yQm9yZGVyIjoiaHNsKDAsIDAlLCA4MyUpIiwiYWN0b3JCa2ciOiIjZWVlIiwiYWN0b3JUZXh0Q29sb3IiOiIjMzMzIiwiYWN0b3JMaW5lQ29sb3IiOiIjNjY2Iiwic2lnbmFsQ29sb3IiOiIjMzMzIiwic2lnbmFsVGV4dENvbG9yIjoiIzMzMyIsImxhYmVsQm94QmtnQ29sb3IiOiIjZWVlIiwibGFiZWxCb3hCb3JkZXJDb2xvciI6ImhzbCgwLCAwJSwgODMlKSIsImxhYmVsVGV4dENvbG9yIjoiIzMzMyIsImxvb3BUZXh0Q29sb3IiOiIjMzMzIiwibm90ZUJvcmRlckNvbG9yIjoiaHNsKDYwLCAxMDAlLCAyMy4zMzMzMzMzMzMzJSkiLCJub3RlQmtnQ29sb3IiOiIjZmZhIiwibm90ZVRleHRDb2xvciI6IiMzMzMiLCJhY3RpdmF0aW9uQm9yZGVyQ29sb3IiOiIjNjY2IiwiYWN0aXZhdGlvbkJrZ0NvbG9yIjoiI2Y0ZjRmNCIsInNlcXVlbmNlTnVtYmVyQ29sb3IiOiJ3aGl0ZSIsInNlY3Rpb25Ca2dDb2xvciI6ImhzbCgyMTAsIDY2LjY2NjY2NjY2NjclLCA3MCUpIiwiYWx0U2VjdGlvbkJrZ0NvbG9yIjoid2hpdGUiLCJzZWN0aW9uQmtnQ29sb3IyIjoiaHNsKDIxMCwgNjYuNjY2NjY2NjY2NyUsIDcwJSkiLCJ0YXNrQm9yZGVyQ29sb3IiOiJoc2woMjEwLCA2Ni42NjY2NjY2NjY3JSwgMzAlKSIsInRhc2tCa2dDb2xvciI6IiMyNmEiLCJ0YXNrVGV4dExpZ2h0Q29sb3IiOiJ3aGl0ZSIsInRhc2tUZXh0Q29sb3IiOiJ3aGl0ZSIsInRhc2tUZXh0RGFya0NvbG9yIjoiIzMzMyIsInRhc2tUZXh0T3V0c2lkZUNvbG9yIjoiIzMzMyIsInRhc2tUZXh0Q2xpY2thYmxlQ29sb3IiOiIjMDAzMTYzIiwiYWN0aXZlVGFza0JvcmRlckNvbG9yIjoiaHNsKDIxMCwgNjYuNjY2NjY2NjY2NyUsIDMwJSkiLCJhY3RpdmVUYXNrQmtnQ29sb3IiOiIjZWVlIiwiZ3JpZENvbG9yIjoiaHNsKDAsIDAlLCA5MCUpIiwiZG9uZVRhc2tCa2dDb2xvciI6IiNiYmIiLCJkb25lVGFza0JvcmRlckNvbG9yIjoiIzY2NiIsImNyaXRCa2dDb2xvciI6IiNkNDIiLCJjcml0Qm9yZGVyQ29sb3IiOiJoc2woMTAuOTA5MDkwOTA5MSwgNzMuMzMzMzMzMzMzMyUsIDQwJSkiLCJ0b2RheUxpbmVDb2xvciI6IiNkNDIiLCJsYWJlbENvbG9yIjoiYmxhY2siLCJlcnJvckJrZ0NvbG9yIjoiIzU1MjIyMiIsImVycm9yVGV4dENvbG9yIjoiIzU1MjIyMiIsImNsYXNzVGV4dCI6IiMxMTExMTEiLCJmaWxsVHlwZTAiOiIjZWVlIiwiZmlsbFR5cGUxIjoiaHNsKDIxMCwgNjYuNjY2NjY2NjY2NyUsIDk1JSkiLCJmaWxsVHlwZTIiOiJoc2woNjQsIDAlLCA5My4zMzMzMzMzMzMzJSkiLCJmaWxsVHlwZTMiOiJoc2woMjc0LCA2Ni42NjY2NjY2NjY3JSwgOTUlKSIsImZpbGxUeXBlNCI6ImhzbCgtNjQsIDAlLCA5My4zMzMzMzMzMzMzJSkiLCJmaWxsVHlwZTUiOiJoc2woMTQ2LCA2Ni42NjY2NjY2NjY3JSwgOTUlKSIsImZpbGxUeXBlNiI6ImhzbCgxMjgsIDAlLCA5My4zMzMzMzMzMzMzJSkiLCJmaWxsVHlwZTciOiJoc2woMzM4LCA2Ni42NjY2NjY2NjY3JSwgOTUlKSJ9fX0)

## Последовательность работы scrap node

```mermaid
sequenceDiagram
    Scrap Node->>+MongoDB: Получаем информацию о задаче которую необходимо выполнить
    MongoDB->>+Scrap Node: Вернули информацию

    Scrap Node->>+Proxy : Совершаем запрос на получение данных через прокси

    Proxy->>+Some Host : Запрос на получение данных от прокси к сайту
    Some Host->>+Proxy : Возвращает ответ

    Proxy->>+Scrap Node : Возвращаем ответ

    Scrap Node->>+Kafka : Сохраняем данные
```

## Последовательность работы Transform node

```mermaid

sequenceDiagram
    Transform Node->>+KafkaDB: Запрос информации по подписке на тему
    KafkaDB->>+Transform Node: Данные

    Transform Node->>+MongoDB : Трасформированные данные заливаем в БД
```

## Жизненный цикл скрапинг приложения

```mermaid
graph LR
  subgraph "Вывод из эксплуатации"
  Node51[Удаление из расписания на запуск] --> Node52[Удаление namespace в кубернитис]
  end

  subgraph "Модификация"
  Node41[Анализ] --> Node411[Формализация требуемых изменений] --> Node421[Модификация]
  Node41 --> Node422[Принятие решения о выводе из эксплуатации]
  end

  subgraph "Мониторинг"
  Node31[Мониторинг запуска] 
  Node32[Мониторинг ошибок] --> Node33[Выявлена ошибка] --> Node34[Переход на этап модификации]
  end

  subgraph "Разработка приложения"
  Node21[Выбор подходящего шаблона] --> Node22[Написание приложения]
  end

  subgraph "Подготовка"
  Node1[Формализация требуемых данных] --> Node2[Поиск ресурсов с интересующими данными] --> Node3[Анализ ресурсов на предмет удобства сбора с них данных] --> Node4[Выбор варианта скрапинга]
  end
```

### Подготовка

#### Формализация требуемых данных
На данном этапе нужно понять потребность клиента:
+ Почему заказчик решил что ему нужны данные ?
+ Почему заказчик решил что ему нужны именно те данные которые он хочет ?
+ Для каких целей будут использоваться данные ?
+ Как часто будут использоваться данные ?

На входе:
+ Обоснование потребности в заказываемых данных.

На выходе:
+ Перечень требуемых данных
+ Формат требуемых данных
+ Ожидаемая частота использования данных
+? Критичность данных, SLA


#### Поиск ресурсов с интересующими данными

На входе:
+ Четкое понимание какие данные нужны.

На выходе:
+ Список ресурсов на которых есть нужные данные

#### Анализ ресурсов на предмет удобства сбора с них данных

На входе:

Список ресурсов из пункта 1.

На выходе:
- Список подходящих под скрапинг ресурсов с наиболее удобным форматом и наименьшим количеством защит и выбранный вариант скрапинга под каждый ресурс.

- Периодичность сбора данных с ресурсов


При подборе ресурсов учесть следующие моменты:

- Формат получаемых данных

Удобнее всего получать уже подготовленные данные которые возвращаются в API.

- Степень защиты
  - Блокировка юзер-агента
  
  - Блокировка по IP (при первичном анализе этот вид защиты может быть не очевиден так как обычные действия пользователя не должны приводить к блокировке по ИП. Как вариант для выявления данного вида блокировки запустить тест на обращение к сайту с одного IP с небольшим интервалом (1,2) секунды в течении 10-15 минут.
  
  - Капча - для доступа к данным требуется решение капчи.
  
  - CSRF защита API
API не принимает запросы без CSRF токена который каждый раз обновляется при обращении к странице сайта.
Подобный механизм защиты обходится в два этапа:
1) Запроса страницы на которой получаем CSRF токен
2) Запрос к API с полученным CSRF токеном

  - Невидимая капча
Некоторые ресурсы используют механизмы идентификации ботов по поведению пользователя на сайте (движения мышкой, скорость действий).
Эта услуга предоставляется некоторыми CDN (например, Cloudflare) и традиционными провайдерами капчи, как Google reCAPTCHA.

Проверить наличие невидимой капчи можно по наличию строки на странице: <script src="https://www.google.com/recaptcha/api.js"></script> 

Актуально только если эмулируем браузер (например с использованием selenium)

  - Бесконечный скроллинг или ленивая загрузка
Некоторые ресурсы загружают только часть данных для отображения и подгружают дополнительные данные при скроллинге страницы до конца. Нужно учитывать этот момент и проверять подгружаются ли еще данные при прокрутке страницы вниз до конца.

Обычно это реализуется вызовом АПИ с индексом подгружаемого фрагмента так что сложностей с выгрузкой обычно не возникает.

#### Выбор варианта скрапинга
- Скрапинг АПИ
Выбираем данный вариант когда уже известен АПИ или если при исследовании сайта обнаружили обращения к АПИ по которому есть интересующие нас данные.

- Скрапинг HTML
Выбираем данный вариант когда сайт отдает статичные данные в html странице не подгружая их из АПИ.

- Скрапинг динамического ресурса
Самый Сложный случай когда для получения обязательно требуется выполнение js скриптов на сайте.

- Скачивание архивов
Проверить есть ли возможность получения дельты данных (некоторые сервисы предоставляют для скачивания не только полный объем данных (который может быть достаточно большим), но и дельты изменений которые обычно существенно меньше.

- Скачивание xlsx файлов
Проверить наличие данных в другом формате более удобном для парсинга (json, xml)


### Подготовка scrap ноды

#### Выбор подходящего докер образа
- Образ для работы с библиотекой requests
- Образ для работы с библиотекой selenium

TODO: Подготовить докер образы.

На входе:
- Выбранные источники данных
- Способы скрапинга источников
- Периодичность скрапинга

На выходе:
- В таблицу скрапинга данных занесена вся требуемая информация
- Скрапинг данных протестирован
 

### Разработка transform ноды
+ Если данные загружаются с АПИ, то в readme.md проекта приложить ссылку на документацию к АПИ, а если ее нет, то явно прописать что документации к АПИ нет.

+ Сохранить в папку tests/sources примеры загружаемых данных
  + Если это АПИ, то ответы всех вызываемых функций
  + Если это html страница, то сохранить саму страницу
  + Если это архив, то приложить архив (Если он не занимает больше 1 Мб. Если больше, то постараться сохранить какую-то часть которой будет достаточно для понимания структуры архива и хранящихся в нем данных.)

+ Написать юнит и интеграционные тесты
+ Добиться 100% покрытия кода тестами
+ Добиться 10 баллов от линтера

На выходе:
- Готовое к работе приложение которое осуществляет сбор необходимых данных
- Настроен CI/CD
- Настроен планировщик выполняющий задачу по расписанию



### Мониторинг

#### Мониторинг запуска
Необходимо осуществлять мониторинг что приложение запускается согласно описанному расписанию.

#### Мониторинг работы приложения
Необходимо осуществлять мониторинг логов на предмет ошибок.

### Модификация
Потребность в модификации возникает либо при возникновении или выявлении ошибок либо при изменении требований к сбору информации.

### Вывод из эксплуатации
- Снять с мониторинга
- Отключить запуск приложения
- Удалить временные объекты которые использует только данное приложение (таблицы БД, файлы, S3 хранилища)
- Удалить объекты кубернитис относящиеся к приложению
- Удалить образы приложения из докер репозитория

## Сопутствующие аспекты

### Этичный скрапинг

- Файл robots.txt
Ознакомиться с файлом /robots.txt для понимания какие разделы сайта открыты для просмотра ботами и какая периодичность скрапинга является допустимой.

- Интервалы между запросами
Не забывать делать интервалы между запросами для того чтобы не положить сайт и не забанили по IP из за превышения лимита по количеству запросов в единицу времени.


### Масштабирование

#### Горизонтальное масштабирование скрап ботов
Механизм при котором существует мастер сервис который управляет масштабированием ботов на имеющихся ресурсах

Зона ответственности мастер сервиса:
- Запуск нового бота на имеющемся ресурсе (выделенный виртуальный сервер или лямбда функция)
- Завершение работы сервиса при определенных условиях (бот перестал лить данные)
- 

Зона ответственности бота:
- Резервирование участка


## Встречаемые ошибки в процессе скрапинга


### Сервер недоступен

Возможные причины:

- Проводятся работы на сервере

- Отсутствует сетевая связность (проблемы на сети)

- Сервер перегружен поступающими запросами

Способы выявления:

- Подготовить отдельный класс для ошибки по таймауту на сайте.

- В коде указать таймаут и в качестве реакции на таймаут выкинуть подготовленное исключение.

### Получен редирект вместо страницы

Возможные причины:

- Сайт поменял структуру

- Страница больше не существует

- Страница закрыта для публичного доступа

### На сайте просрочился SSL сертификат


## Общий алгоритм скрапинга

1. Загрузка
2. Парсинг
3. Валидация
4. Сохранение

### Загрузка
 На данном этапе осуществляется получение контента в котором содержатся искомые данные
+ Для работы с json/xml REST API используем библиотеку requests
+ Для работы с SOAP API используем библиотеку zeep

+ Для загрузки статичных html страниц используем библиотеку requests
Плюсы:
+ Скорость работы
Минусы:
+ Невозможно получить конечный DOM который формируется после исполнения js скриптов

+ Для выполнения js скриптов на странице используем библиотеку selenium
Использовать только как крайнюю меру. В большинстве случаев данные можно получить без необходимости использовать полноценный механизм загрузки html страницы внутри 

Плюсы:
+ Можно эмулировать поведение пользователя на сайте
+ Можно сделать скриншот загруженной страницы
Минусы:
+ Очень медленно


### Парсинг

+ Для парсинга html страницы используем следующий набор инструментов:
+ Регулярные выражения
+ Библиотека beautifulsoup
+ Очистка данных (приведение к нижнему регистру, Удаление лишних пробелов, )
+ Нечеткий поиск : Если текст периодически меняется (например в нем может содержаться дата/время) то как вариант можно использовать нечеткий поиск (например расстояние Левинштейна)

### Валидация

На данном шаге осуществляем проверку полученных данных:
+ Что вообще что-то загрузилось
+ Что тип полученных данных соответствует тому что мы ждем
+ Если значение можно проверить (по количеству символов, по какому-либо алгоритму) то проверяем значения 
Например 

### Сохранение
На данном шаге осуществляется запись подготовленной и валидированной информации в хранилище.
+ Для записи в БД Монго используется библиотека pymongo



## Особенности скрапинга 

### Скрапинг статичной html страницы


### Скрапинг динамической html страницы




## Литература

https://habr.com/ru/post/353348/

https://habr.com/ru/post/323202/

https://habr.com/ru/post/442258/